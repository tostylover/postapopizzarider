using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerCollosion : MonoBehaviour
{
    public GameObject deathMenuUI;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        deathMenuUI.SetActive(true);
        Time.timeScale = 0f;
    }
    
 }
