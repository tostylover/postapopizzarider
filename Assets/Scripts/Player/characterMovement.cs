using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class characterMovement : MonoBehaviour
{
    GameObject parent;
    Vector2 movingPosition;
    public float yPos;
    public float changeSpeed;

    void Start()
    {
        movingPosition = new Vector2(-5, yPos);
        parent = this.gameObject;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.W) && yPos < -1)
        {
            yPos++;
            movingPosition.y = yPos;
            StartCoroutine(MoveToPoint());
        }

        if (Input.GetKeyDown(KeyCode.S) && yPos > -3)
        {
            yPos--;
            movingPosition.y = yPos;
            StartCoroutine(MoveToPoint());
        }

        if (transform.position.y == -1)
        {
            this.gameObject.GetComponent<SpriteRenderer>().sortingOrder = -1;
        }
        else
        {
            this.gameObject.GetComponent<SpriteRenderer>().sortingOrder = 1;
        }

    }

    IEnumerator MoveToPoint()
    {
        float waitTime = 0.04f;

        while (true)
        {
            float step = changeSpeed * waitTime;

            transform.position = Vector3.MoveTowards(transform.position, movingPosition, step);
            yield return new WaitForSeconds(waitTime);
            if (parent.transform.position.y == movingPosition.y)
            {
                yield break;
            }
        }
        
    }
    
}