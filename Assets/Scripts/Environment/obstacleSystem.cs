using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class obstacleSystem : MonoBehaviour
{
    [SerializeField] GameObject[] obstaclesLevelOne;

    [SerializeField] private float delay;
    
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(obstaclesSpawner());   
    }

    void generateObstacle() 
    {
        int randomObstacle = Random.Range(0, obstaclesLevelOne.Length);

        GameObject obstacle = Instantiate(obstaclesLevelOne[randomObstacle]);
        obstacle.transform.position = new Vector3(10, -2, 0);

    }

    IEnumerator obstaclesSpawner()
    {
        while (true)
        {
            generateObstacle();
            yield return new WaitForSeconds(delay);
        }
    }
}
