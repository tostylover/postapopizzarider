using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class obstacleSpeed : MonoBehaviour
{
    private Rigidbody2D obstacleRigidbody;
    [SerializeField]private float speed;

    // Start is called before the first frame update
    void Start()
    {
        obstacleRigidbody = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        obstacleRigidbody.velocity = -Vector2.right * speed;

        if (transform.position.x <= -10) 
        {
            Destroy(this.gameObject);
        }
    }
}
