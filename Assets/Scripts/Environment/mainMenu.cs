using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class mainMenu : MonoBehaviour
{
    [SerializeField]
    private Button playButton, quitButton, optionsButton;

    private void Start()
    {
        playButton.onClick.AddListener(()=> PlayGame());
        quitButton.onClick.AddListener(() => QuitGame());
    }

    public void PlayGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void QuitGame()
    {
        Debug.Log("Quit!!!");
        Application.Quit();
    }
}